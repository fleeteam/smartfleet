/* @flow */

import _ from 'lodash';

import {
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAILURE,
  USER_LOGIN_REQUEST,
  USER_LOGOUT_SUCCESS,
  USER_CHECK_DONE,
} from './user_action';

import type { User, Action } from '../types';

type State = User;

const initialState = {
  username: '',
  password: '',
  token: null,
  actionStatus: null,
};

export default (state: State = initialState, action: Action): State => {
  switch (action.type) {
    case USER_CHECK_DONE:
      return _.assign({}, state, { actionStatus: USER_LOGIN_REQUEST });
    case USER_LOGIN_REQUEST:
      return _.assign({}, state, { actionStatus: USER_LOGIN_REQUEST });
    case USER_LOGIN_FAILURE:
      return _.assign({}, state, {
        actionStatus: USER_LOGIN_FAILURE,
        err: action.err,
      });
    case USER_LOGIN_SUCCESS:
      return _.assign({}, state, {
        actionStatus: USER_LOGIN_SUCCESS,
        token: action.token,
        err: null,
      });
    case USER_LOGOUT_SUCCESS:
      return _.assign({}, state, {
        actionStatus: USER_LOGOUT_SUCCESS,
        token: action.token,
        err: null,
      });
    default:
      return state;
  }
};
