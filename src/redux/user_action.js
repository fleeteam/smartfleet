import localforage from 'localforage';
import type {
  Dispatch,
  GetState,
  ThunkAction,
} from '../types';

export const USER_CHECK_AUTH = 'USER_CHECK_AUTH';
export const USER_CHECK_DONE = 'USER_CHECK_DONE';
export const USER_LOGIN_SUCCESS = 'USER_LOGIN_SUCCESS';
export const USER_LOGIN_FAILURE = 'USER_LOGIN_FAILURE';
export const USER_LOGIN_REQUEST = 'USER_LOGIN_REQUEST';
export const USER_LOGOUT_REQUEST = 'USER_LOGOUT_REQUEST';
export const USER_LOGOUT_SUCCESS = 'USER_LOGOUT_SUCCESS';

export const doLogin = (user): ThunkAction =>
  (dispatch: Dispatch, getState: GetState, axios: any) => {
    dispatch({ type: USER_LOGIN_REQUEST });
    return axios.post('/auth/login', {
      username: user.username,
      password: user.password,
    })
    .then((res) => {
      if (res.data.success) {
        localforage.setItem('token', res.data.token).then(() => {
          return localforage.getItem('token');
        }).then((value) => {
          dispatch({ type: USER_LOGIN_SUCCESS, token: value });
        }).catch((err) => {
          dispatch({ type: USER_LOGIN_FAILURE, err });
        });
      } else {
        dispatch({ type: USER_LOGIN_FAILURE, err: res.data.message });
      }
    })
    .catch((err) => {
      dispatch({ type: USER_LOGIN_FAILURE, err });
    });
  };

export const checkAuth = (): ThunkAction =>
  (dispatch: Dispatch) => {
    dispatch({ type: USER_CHECK_AUTH });
    localforage.getItem('token').then((data) => {
      dispatch({ type: USER_CHECK_DONE, token: data });
    });
  };

export const doLogout = (): ThunkAction =>
  (dispatch: Dispatch) => {
    dispatch({ type: USER_LOGOUT_REQUEST });
    localforage.removeItem('token', () => {
      dispatch({ type: USER_LOGOUT_SUCCESS });
    });
  };
