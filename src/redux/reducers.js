/* @flow */

import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';

import user from './user_reducer';

export default combineReducers({
  user,
  router,
});
