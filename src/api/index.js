const express = require('express');

import lessonplans from './lessonplans';

const router = express.Router();

router.use('/lessonplans', lessonplans);

module.exports = router;
