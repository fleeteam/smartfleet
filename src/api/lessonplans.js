import express from 'express';

const router = express.Router();

router.get('/', (req, res) => {
  res.send('Lesson Plans');
});

module.exports = router;
