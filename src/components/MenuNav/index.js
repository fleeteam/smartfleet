import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import { Navbar, Nav, NavItem } from 'react-bootstrap';


class MenuNav extends PureComponent {

  handleSelect = (eventKey) => {
    event.preventDefault();
    alert(`selected ${eventKey}`);
  }

  render() {
    return (
      <Navbar inverse collapseOnSelect>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to="" />
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav activeKey="1" onSelect={this.handleSelect}>
            <NavItem eventKey={1} href="#">Overview</NavItem>
            <NavItem eventKey={2} href="#">Objectives</NavItem>
            <NavItem eventKey={3} href="#">Subject Matter</NavItem>
            <NavItem eventKey={4} href="#">Procedures</NavItem>
            <NavItem eventKey={5} href="#">Evaluation</NavItem>
            <NavItem eventKey={6} href="#">Assignment</NavItem>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

export default MenuNav;
