module.exports = {
  host: process.env.NODE_HOST || 'localhost', // Define your host from 'package.json'
  port: process.env.PORT,
  vesApi: 'http://dev-api-core.vibalgroup.com/api',
  jwtSecret: 'vibal-lpp2017',
  app: {
    htmlAttributes: { lang: 'en' },
    title: 'Lesson Planner',
    titleTemplate: 'React Cool Starter - %s',
    meta: [
      {
        name: 'description',
        content: 'The best react universal starter boilerplate in the world.',
      },
    ],
  },
};
