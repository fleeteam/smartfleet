import jwt from 'jsonwebtoken';
import axios from 'axios';
import { Strategy as PassportLocalStrategy } from 'passport-local';
import config from '../../config';

/**
 * Return the Passport Local Strategy object.
 */
module.exports = new PassportLocalStrategy({
  usernameField: 'username',
  passwordField: 'password',
  session: false,
  passReqToCallback: true
}, (req, username, password, done) => {
  const userData = {
    username: username.trim(),
    password: password.trim()
  };

  axios.post(`${config.vesApi}/Authenticate/UserAccount`, {
    UserName: req.body.username,
    Password: req.body.password,
    RememberMe: req.body.rememberme || false,
  })
  .then((apiRes) => {
    if (apiRes.data.StatusCode === 3) {
      const payload = {
        user: {
          vesUserId: apiRes.data.Result.VESUserId,
          empNumber: apiRes.data.Result.EmployeeNumber
        }
      };

      // create a token string
      const token = jwt.sign(payload, config.jwtSecret);
      const data = {
        username: apiRes.data.Result.UserName
      };

      return done(null, token, data);
    }
    const error = new Error('Incorrect email or password');
    error.name = 'IncorrectCredentialsError';
    return done(error);
  })
  .catch((err) => {
    const error = new Error('Server Error');
    error.name = 'ServerError';
    return done(error)
  });

  // find a user by email address
  // return User.findOne({ email: userData.email }, (err, user) => {
  //   if (err) { return done(err); }
  //
  //   if (!user) {
  //     const error = new Error('Incorrect email or password');
  //     error.name = 'IncorrectCredentialsError';
  //
  //     return done(error);
  //   }
  //
  //   // check if a hashed user's password is equal to a value saved in the database
  //   return user.comparePassword(userData.password, (passwordErr, isMatch) => {
  //     if (err) { return done(err); }
  //
  //     if (!isMatch) {
  //       const error = new Error('Incorrect email or password');
  //       error.name = 'IncorrectCredentialsError';
  //
  //       return done(error);
  //     }
  //
  //     const payload = {
  //       sub: user._id
  //     };
  //
  //     // create a token string
  //     const token = jwt.sign(payload, config.jwtSecret);
  //     const data = {
  //       name: user.name
  //     };
  //
  //     return done(null, token, data);
  //   });
  // });
});
