/* @flow */

import type { Store as ReduxStore } from 'redux';

export type User = {
  user: {
    username: string,
    password: string,
  };
  err: any
};

export type Reducer = {
  user: User,
  router: any,
};

export type Action =
  { type: 'USER_LOGOUT_SUCCESS', data: any } |
  { type: 'USER_LOGOUT_REQUEST', data: any } |
  { type: 'USER_LOGIN_REQUEST', data: any } |
  { type: 'USER_LOGIN_SUCCESS', data: any } |
  { type: 'USER_LOGIN_FAILURE', err: any };

export type Store = ReduxStore<Reducer, Action>;
// eslint-disable-next-line no-use-before-define
export type Dispatch = (action: Action | ThunkAction | PromiseAction | Array<Action>) => any;
export type GetState = () => Object;
export type ThunkAction = (dispatch: Dispatch, getState: GetState) => any;
export type PromiseAction = Promise<Action>;
