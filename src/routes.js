/* @flow */
import Login from './containers/Login';
import DashboardTest from './containers/Dashboard';
import NotFoundPage from './containers/NotFound';

export default [
  {
    path: '/',
    exact: true,
    component: Login,
  },
  {
    path: '/dashboard',
    exact: true,
    component: DashboardTest,
  },
  {
    path: '*',
    component: NotFoundPage,
  },
];
