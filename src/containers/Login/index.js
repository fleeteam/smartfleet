import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import type { Connector } from 'react-redux';
import Helmet from 'react-helmet';
import { Redirect } from 'react-router-dom';
import { Alert, Button, FormGroup, FormControl, ControlLabel } from 'react-bootstrap';
import styles from './styles.scss';

import * as action from '../../redux/user_action';
import type { User as UserType, Dispatch, Reducer } from '../../types';

type Props = {
  user: UserType,
  doLogin: (string, string) => void,
  checkAuth: () => void,
};

class Login extends PureComponent {
  props: Props;

  constructor(props) {
    super(props);

    this.state = {
      username: props.user.username,
      password: props.user.password,
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.props.checkAuth();
  }

  onSubmit(e) {
    e.preventDefault();
    this.props.doLogin({
      username: this.state.username,
      password: this.state.password,
    });
  }

  render() {
    let err = null;
    if (this.props.user.err) {
      err = (<Alert bsStyle="danger" onDismiss={this.handleAlertDismiss}>
        <p>{this.props.user.err}</p>
      </Alert>);
    }
    return this.props.user.token != null ? <Redirect to={{ pathname: '/dashboard' }} /> :
    (
      <div className={styles.Login}>
        <Helmet title="Login" />
        <form>
          <h2 className="card-heading">Login</h2>
          {err}
          <FormGroup
            controlId="formBasicText"
          >
            <ControlLabel>Username</ControlLabel>
            <FormControl
              type="text"
              name="username"
              onChange={(e) => { this.setState({ username: e.target.value }); }}
              value={this.state.username}
            />
          </FormGroup>
          <FormGroup
            controlId="formBasicText"
          >
            <ControlLabel>Password</ControlLabel>
            <FormControl
              type="password"
              name="password"
              onChange={(e) => { this.setState({ password: e.target.value }); }}
              value={this.state.password}
            />
          </FormGroup>

          <Button bsStyle="primary" type="submit" onClick={this.onSubmit}>LOGIN</Button>
        </form>
      </div>
    );
  }
}

const connector: Connector<{}, Props> = connect(
  ({ user }: Reducer) => ({ user }),
  (dispatch: Dispatch) => ({
    doLogin: (username: string, password: string) => dispatch(action.doLogin(username, password)),
    checkAuth: () => dispatch(action.checkAuth()),
  }),
);

export default connector(Login);
