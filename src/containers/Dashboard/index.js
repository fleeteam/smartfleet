import React, { Component } from 'react';
import { connect } from 'react-redux';
import type { Connector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as action from '../../redux/user_action';
import type { User, Dispatch, Reducer } from '../../types';

type Props = {
  user: User,
  doLogout: () => void
};

class Dashboard extends Component {
  props: Props;

  constructor(props) {
    super(props);
    this.logout = this.logout.bind(this);
  }

  logout() {
    this.props.doLogout();
  }

  render() {
    return (
      this.props.user.token ? <div> This is a protected route
        <button onClick={this.logout}>Logout</button>
      </div> : <Redirect to={{ pathname: '/' }} />
    );
  }
}

const connector: Connector<{}, Props> = connect(
  ({ user }: Reducer) => ({ user }),
  (dispatch: Dispatch) => ({
    doLogout: () => dispatch(action.doLogout()),
  }),
);

export default connector(Dashboard);
